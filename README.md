# TaskTracker

Для начала убедитесь, что у вас стоят: python3 & pip, venv, [Node.js & NPM](https://nodejs.org/en/download), axios, mysql

При клонировании:
```
python3 -m venv venv 
source venv/bin/activate
pip install -r requirements.txt
npm install -g vue-cli
```

Запуск виртуального окружения:
```
source venv/bin/activate
```
или
```
./venv/bin/Activate.ps1
```

Проверка бэка:
```
python manage.py runserver
```

Проверка фронта:
```
cd frontend
npm run dev
```
